	<!-- container -->
			<!-- header -->
			<div id="home" class="header">
				<div class="container">
				<!-- top-hedader -->
				<div class="top-header">
					<!-- /logo -->
					<!--top-nav---->
					<div class="top-nav">
					<div class="navigation">
					<div class="logo">
						<h1><a href="/"><span>P</span>REFACE</a></h1>
					</div>
					<div class="navigation-right">
						<span class="menu"><img src="images/menu.png" alt=" " /></span>
						<nav class="link-effect-3" id="link-effect-3">
							<ul class="nav1 nav nav-wil">
								<li class="active"><a data-hover="Home" href="/">Home</a></li>
								<li><a class="scroll" data-hover="About" href="#about">About</a></li>
								<li><a class="scroll" data-hover="Services" href="#services" >Services</a></li>
								<li><a class="scroll" data-hover="Portfolio" href="#port">Portfolio</a></li>
								<li><a class="scroll" data-hover="Blog" href="#blogs">Blog</a></li>
								<li><a class="scroll" data-hover="Contact" href="#contact">Contact</a></li>
							</ul>
						</nav>
