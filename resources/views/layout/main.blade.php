<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>{{ $title }}</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
 <!-- Custom Theme files -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
	<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- webfonts -->
 <!---- start-smoth-scrolling---->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
 <!---- start-smoth-scrolling---->
</head>
	<body>
	@include('partials.navbar');
							<!-- script-for-menu -->
								<script>
								   $( "span.menu" ).click(function() {
									 $( "ul.nav1" ).slideToggle( 300, function() {
									 // Animation complete.
									  });
									 });
								</script>
							<!-- /script-for-menu -->
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- /top-hedader -->
				</div>
			<div class="banner-info">
				<div class="col-md-7 header-right">
					<h1>Hi !</h1>
					<h6>EVERYONE</h6>
					<ul class="address">

					<li>
							<ul class="address-text">
								<li><b>NAMA</b></li>
								<li>{{ $nama }}</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>TGL LAHIR</b></li>
								<li>01-08-2001</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>NO TELP</b></li>
								<li>{{ $no }}</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>ALAMAT </b></li>
								<li>{{ $alamat }}</li>
							</ul>
						</li>
						<li>
							<ul class="address-text">
								<li><b>E-MAIL </b></li>
								<li><a href="mailto:hananjoyo01@gmail.com"> {{ $email }}</a></li>
							</ul>
						</li>

					</ul>
				</div>
				<div class="col-md-5 header-left">
					<img src="images/img1.jpg" alt="">
				</div>
				<div class="clearfix"> </div>

		      </div>
			</div>
		</div>
	</div>
			<!-- about -->
			<div id="about" class="about">
				<div class="col-md-6 about-left">
					<div id="owl-demo1" class="owl-carousel owl-carousel2">
					                <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>{{ $nama }}</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara,dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Portfolio </a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					                  <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>{{ $nama }}</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara,dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Work</a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					                  <div class="item">
					                	<div class="about-left-grid">
											<h2>Hi! Saya <span>{{ $nama }}</span></h2>
											<p>Nama lengkap saya Adi Hananjoyo dan saya berumur 20 tahun, saya berasal dari Negara dan dapat berkomunikasi dengan beberapa bahasa dengan baik, sekarang saya sedang menempuh pendidikan di prodi S1 Sistem Informasi Undiksha. Saya memiliki hobi bermain game.</p>
											<ul>
												<li><a class="a-btn-a scroll" href="#port">My Work</a></li>
												<li><a class="a-btn-h scroll" href="#contact">Hire Me</a></li>
											</ul>
										</div>
					                </div>
					</div>
				</div>
				<div class="col-md-6 about-right">

				</div>
				<div class="clearfix"> </div>
							<link href="css/owl.carousel.css" rel="stylesheet">
							    <script src="js/owl.carousel.js"></script>
			<script>
				$(document).ready(function() {
					$("#owl-demo1").owlCarousel({
						items : 1,
						lazyLoad : false,
						autoPlay : true,
						navigation : false,
						navigationText :  false,
						pagination : true,
					});
				});
			</script>
			<!-- Feedback -->
			<script>
				$(document).ready(function() {
					$("#owl-demo3").owlCarousel({
						items : 1,
						lazyLoad : false,
						autoPlay : true,
						navigation : false,
						navigationText :  true,
						pagination :true,
					});
				});
			</script>
			</div>
			<!-- /about -->
			<!-- services -->
			<div id="services" class="services">
				<div class="container">
					<div class="service-head one text-center ">
						<h4>Apa yang saya pelajari </h4>
						<h3> <span>Keahlian</span></h3>
						<span class="border two"></span>
					</div>
					<!-- services-grids -->
					<div class="wthree_about_right_grids w3l-agile">
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-pencil"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4>CSS</h4>
						<p>CSS adalah kepanjangaan dari Cascading Style Sheets yang berguna untuk menyederhanakan proses pembuatan website dengan mengatur elemen yang tertulis di bahasa markup.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-cog"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4>HTML</h4>
						<p>Hypertext Markup Language adalah bahasa markah standar untuk dokumen yang dirancang untuk ditampilkan di peramban internet. Ini dapat dibantu oleh teknologi seperti Cascading Style Sheets dan bahasa scripting seperti JavaScript dan VBScript.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-leaf"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4>LARAVEL</h4>
						<p>Laravel adalah kerangka kerja aplikasi web berbasis PHP yang sumber terbuka, menggunakan konsep Model-View-Controller.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 wthree_about_right_grid">
					<div class="col-xs-4 wthree_about_right_grid_left">
						<div class="hvr-rectangle-in">
							<i class="glyphicon glyphicon-gift"></i>
						</div>
					</div>
					<div class="col-xs-8 wthree_about_right_grid_right">
						<h4>PHP</h4>
						<p>PHP: Hypertext Preprocessor adalah bahasa skrip yang dapat ditanamkan atau disisipkan ke dalam HTML. PHP banyak dipakai untuk memprogram situs web dinamis.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="clearfix"> </div>
			</div>

					<!-- services-grids -->
				</div>
			</div>
			<!-- services -->
			<!--work-experience-->

	<!--//work-experience-->

		<!-- portfolio -->
<div class="portfolio" id="port">
				<div class="service-head text-center">
						<h4>Prestasi</h4>
						<h3><span>PORTFOLIO</span></h3>
						<span class="border"></span>
					</div>
			<div class="portfolio-grids">
				<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
				<script type="text/javascript">
									$(document).ready(function () {
										$('#horizontalTab').easyResponsiveTabs({
											type: 'default', //Types: default, vertical, accordion
											width: 'auto', //auto or any width like 600px
											fit: true   // 100% fit in a container
										});
									});

				</script>
				<div class="sap_tabs">
					<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						<ul class="resp-tabs-list">
							<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Dokumentasi</span></li>

						</ul>
						<div class="resp-tabs-container">
							<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal1" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P1.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal3" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P2.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal2" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P3.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal4" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P4.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal5" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P5.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal6" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P6.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal7" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P7.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal8" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P8.jpg" alt="">

										</a>

								</div>
								<div class="clearfix"></div>
							</div>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	<!-- //portfolio -->
    <!-- top-grids -->
				<div class="blog" id="blogs">
					<div class="container">
						<div class="service-head text-center">
						<h4>BLOGS</h4>
						<h3>MY <span>BLOGS</span></h3>
						<span class="border one"></span>
					</div>
					   <div class="news-grid w3l-agile">
					    <div class="col-md-6 news-img">
						  <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b1.jpg" alt=" " class="img-responsive"></a>

						</div>
					    <div class="col-md-6 news-text">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
							<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>

						<div class="clearfix"></div>
					 </div>
					  <div class="news-grid">

					    <div class="col-md-6 news-text two">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
								<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>
						<div class="col-md-6 news-img two">
						   <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b2.jpg" alt=" " class="img-responsive"></a>

						</div>
						<div class="clearfix"></div>
					 </div>
					  <div class="news-grid">
					    <div class="col-md-6 news-img">
						  <a href="#" data-toggle="modal" data-target="#myModal1"> <img src="images/b3.jpg" alt=" " class="img-responsive"></a>

						</div>
					    <div class="col-md-6 news-text">
						   <h3> <a href="#" data-toggle="modal" data-target="#myModal1">HERE GOES AN AWESOME BLOG TITLE</a></h3>
							<ul class="news">
								<li><i class="glyphicon glyphicon-user"></i> <a href="#">Admin</a></li>
								<li><i class="glyphicon glyphicon-comment"></i> <a href="#">2 Comments</a></li>
								<li><i class="glyphicon glyphicon-heart"></i> <a href="#">50 Likes</a></li>
								<li><i class="glyphicon glyphicon-tags"></i> <a href="#">3 Tags</a></li>
							</ul>
							<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
							<a href="#" data-toggle="modal" data-target="#myModal1" class="read hvr-shutter-in-horizontal">Read More</a>

						</div>

						<div class="clearfix"></div>
					 </div>
					</div>
				</div>
				<!-- top-grids -->
	<!-- /blog-pop-->
			<div class="modal ab fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog about" role="document">
					<div class="modal-content about">
						<div class="modal-header">
							<button type="button" class="close ab" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body about">
								<div class="about">

									  <div class="about-inner">

									      <img src="images/b3.jpg" alt="about"/>
										     <h4 class="tittle">A Fews words about us</h4>
										   <p>Lorem ipsum dolor sit amet Integer gravida,Lorem ipsum dolor sit amet Integer gravida velit,Ming sits in the corner the whole day. She's into crochet. quis dolor tristiqumsan.Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. velit quis dolor tristiqumsan.</p>
										    <p>Lorem ipsum dolor sit amet. Integer gravida velit quis dolor tristiqumsan.anteposuerit litterarum formas humanitatis per seacula amet Integer gravida velit. </p>
									  </div>

								</div>
						</div>
					</div>
				</div>
			</div>
			<!-- //blog-pop-->

			<!-- /header -->
<div class="footer" id="contact">
	<div class="container">
	<div class="service-head one text-center">
						<h4>CONTACT ME</h4>
						<h3>GET <span>IN TOUCH WITH ME</span></h3>
						<span class="border two"></span>
					</div>
		<div class="mail_us">
			<div class="col-md-6 mail_left">
				<div class="contact-grid1-left">
					<div class="contact-grid1-left1">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						<h4>Contact By Email</h4>
						<ul>
							<li>Mail: <a href="mailto:hananjoyo01@gmail.com">{{ $email }}</a></li>
						</ul>
					</div>
				</div>
					<div class="contact-grid1-left">
						<div class="contact-grid1-left1">
							<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
							<h4>Contact By Phone</h4>
							<ul>
								<li>Phone: {{ $no }}</li>
							</ul>
						</div>
					</div>
					<div class="contact-grid1-left">
						<div class="contact-grid1-left1">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							<h4>Looking For Address</h4>
							<ul>
								<li>Address: {{ $alamat }}</li>
								<li>Bali,Indonesia</li>
							</ul>
						</div>
					</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 mail_right">
				<form action="#" method="post">
					<input type="text" name="Name" value="Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" required="">
					<input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
					<input type="text" name="Mobile number" value="Mobile number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mobile number';}" required="">
					<textarea name="Message..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>
					<input type="submit" value="Send">

				</form>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="copy_right text-center">
			<p>&copy; 2016 Preface . All rights reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts.</a></p>
			 <ul class="social-icons two">
							<li><a href="https://twitter.com/ananjoyo01" class="twt"> </a></li>
							<li><a href="https://www.facebook.com/hanan.braleon" class="fb"> </a></li>
							<li><a href="mailto:hananjoyo01@gmail.com" class="in"> </a></li>
							<li><a href="#" class="dott"> </a></li>
						</ul>
		</div>
	</div>
</div>
			<!-- //footer -->
		<!-- /container -->
		<div class="portfolio-modal modal fade slideanim" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>Juara 1 DIES NATALIS UNDIKSHA</h3>
                        <img src="images/P1.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 1 ANJAY ORGANIZER SEASON 1</h3>
                        <img src="images/P3.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 2 RMJS TOUR</h3>
                        <img src="images/P2.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>HUARA 1 DIES NATALIS UNDIKSHA</h3>
                        <img src="images/P4.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 2 MLCC BALI</h3>
                        <img src="images/P5.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 1 KINGS TOURNAMENT</h3>
                        <img src="images/P6.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 3 MAX ONLINE TOURNAMENT</h3>
                        <img src="images/P7.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
						<h3>JUARA 1 GREDEG TOURNAMENT</h3>
                        <img src="images/P8.jpg" class="img-responsive img-centered" alt="">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="portfolio-modal modal fade slideanim" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content port-modal">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl"></div>
            </div>
        </div>
        <div class="container">
			<div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <div class="modal-body">
                        <img src="images/pic1.jpg" class="img-responsive img-centered" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear'
								 		};
										*/

										$().UItoTop({ easingType: 'easeOutQuart' });

									});

@yield('container')
								</script>
								<!--end-smooth-scrolling-->
<!-- //for bootstrap working -->
	<script src="js/bootstrap.js"></script>


	</body>
</html>

