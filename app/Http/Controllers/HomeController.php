<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        return view('home',[
            "title"=> "home",
            "nama" => "Adi Hananjoyo",
            "email" => "hananjoyo01@gmail.com",
            "no" => "+62 82147245723",
            "alamat" => "JLN. NUSA INDAH RAYA NO.52 NEGARA"
        ]);
    }
}
